
# XNAT-HPC Bridge: Automated File Transfer Between XNAT and SLURM-enabled HPC

## Overview

This project facilitates the seamless transfer of files between an **XNAT** server and a **SLURM-enabled HPC** system using an automated method. The solution leverages a Python script and a shell script to enable users to download metadata and imaging data from an XNAT project and store it in the target directory on the HPC system, enabling integration with SLURM for further processing or analysis.

### Components

1. **hpc-xnat.py**: A Python script that handles the download of XNAT project data and transfers it to the HPC

The script does the following:
- Downloads metadata (project and subject layers) from the XNAT project.
- Downloads the image data associated with the project.
- Transfers the data to a target directory on the HPC system under `/scratch/users/<hpc-username>/.cache-xnat`.
   
2. **xnat-hpc.sh**: A shell script used to initiate the file transfer procedure and to execute the user's code afterwards

The script does the following:
- Installs the needed packages click and XN (https://gitlab.gwdg.de/medinfpub/mi-xnat/extensions/clients/xn) for runnung the transfer
- Executes the Python script **hpc-xnat.py** for the actual file exchange
- Executes the user's analysis script


---

## Requirements

- Access to an XNAT instance
- SLURM-enabled HPC system
- anaconda3 installed on the SLURM-enabled HPC


---

## Setup
- log-in into the HPC environment
- Place the Python script `hpc-xnat.py` and shell script `xnat-hpc.sh` in the same directory on the HPC system.
- Add project id and host to the file `hpc-xnat.sh` at `#EDIT HERE XNAT INFORMATION`
- Modify the shell script `xnat-hpc.sh` to suit your specific SLURM job requirements
- Modify the shell script `xnat-hpc.sh` to run your code at `#EDIT HERE SCRIPTS TO EXECUTE`


---

## Usage

```bash
sbatch  hpc-xnat.sh <xnat-username> <xnat-password>
```

- `<xnat-username>`: The XNAT username to login into the system.
- `<xnat-username>`: The XNAT password to login into the system. Could also be a hash if configured.


---
## License



---

## Contact

For any questions or support, please reach out to the project maintainers at philip.zaschke@med.uni-goettingen.de
