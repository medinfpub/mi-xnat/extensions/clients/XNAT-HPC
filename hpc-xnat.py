import click
from xn import cli
import os
import time

def time_convert(sec):
  mins = sec // 60
  sec = sec % 60
  hours = mins // 60
  mins = mins % 60
  print("Time Lapsed = {0}:{1}:{2}".format(int(hours),int(mins),sec))

def _download_metadata(session, xnat_path, target_dir):
    labels = session.get(f'{xnat_path}/resources').json()['ResultSet']['Result']
    labels = [label['label'] for label in labels]
    for label in labels:
        file_uris = session.get(f'{xnat_path}/resources/{label}/files').json()['ResultSet']['Result']
        file_uris = [file_uri['URI'] for file_uri in file_uris]

        for file_uri in file_uris:
            file = session.get(file_uri) 
            file_name = file_uri.split('/')[-1]
            os.makedirs(os.path.dirname(f'{target_dir}/resources/{label}/files/{file_name}'), exist_ok=True)
            with open(f'{target_dir}/resources/{label}/files/{file_name}', 'a+') as f:
                f.write(file.content.decode('utf8'))

@click.command()
@click.argument('project-id', type=str)
@click.argument('hpc-username', type=str)
@click.pass_context
def hpcxnatdownload(ctx, project_id, hpc_username, *args, **kwargs):
    start_time = time.time()
    xnat_path = '/data/projects/'
    target_dir = '/scratch/users/'
    target_folder = '/.cache-xnat'


    session = ctx.obj

    xnat_path = xnat_path + project_id
    target_dir = target_dir + hpc_username + target_folder

    #get project name with id
    project_name = session.get(xnat_path+'?format=json').json()['items'][0]['data_fields']['name']
    
    #check if project already there using project name
    if os.path.exists(target_dir+'/'+project_name):
        print('found already existing project: '+ project_name)
    else:
        #download project
        print('downloading metadata for project '+project_id+'...')
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        
        #Download Metadata Project Layer
        _download_metadata(session, f'{xnat_path}', f'{target_dir}/{project_name}')

        #Download Metadata subject layer
        subjects = session.get(f'{xnat_path}/subjects').json()['ResultSet']['Result']
        subjects = [subject['label'] for subject in subjects]
        for subject in subjects:
            _download_metadata(session, f'{xnat_path}/subjects/{subject}', f'{target_dir}/{project_name}/{subject}')
        print('downloading image data for project '+project_id+'...')    
        session.download_dir(f'{xnat_path}', target_dir)
        
    end_time = time.time()
    time_lapsed = end_time - start_time
    time_convert(time_lapsed)
  



cli.add_command(hpcxnatdownload)


if __name__ == '__main__':
    cli()