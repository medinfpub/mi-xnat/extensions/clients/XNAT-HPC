#!/bin/bash
#SBATCH -p medium

#SBATCH -t 2-00:00:00
#SBATCH -o XNAT-HPC-%j.out


module load anaconda3
#module load cuda
pip install click
pip install git+https://gitlab.gwdg.de/medinfpub/mi-xnat/xnat/clients/xn.git@backport-python3.8



###
#EDIT HERE XNAT INFORMATION
project_id='project_id'
host='https://xnat-dev.gwdg.de/'
###

start_date1=$(date +%F)
start_date2=$(date +%T)


/usr/users/$USER/.local/bin/xn --host $host --user $1 --password $2 request --method PUT --query wrk:workflowData/id $project_id --query wrk:workflowData/pipeline_name HPC_Job --query wrk:workflowData/data_type xnat:mrSessionData --query wrk:workflowData/status Progress --query wrk:workflowData/launch_time $start_date1\ $start_date2 /data/workflows
python hpc-xnat.py --host $host --user $1 --password $2 hpcxnatdownload $project_id $USER

 

###
#EDIT HERE SCRIPTS TO EXECUTE
python /scratch/users/$USER/helloworld.py


###


/usr/users/$USER/.local/bin/xn --host $host --user $1 --password $2 request --method PUT --query wrk:workflowData/id $project_id --query wrk:workflowData/pipeline_name HPC_Job --query wrk:workflowData/data_type xnat:mrSessionData --query wrk:workflowData/status Complete --query wrk:workflowData/launch_time $start_date1\ $start_date2 /data/workflows